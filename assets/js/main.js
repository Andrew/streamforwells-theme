document.addEventListener("DOMContentLoaded", function() {
    //
    // Full width elements
    //

    var fullWidthElements = document.querySelectorAll(".sfwc-section");

    var updateFullWidthElements = function() {
        fullWidthElements.forEach(function(section) {
            section.style.width = document.body.clientWidth + "px";
        });
    };

    updateFullWidthElements();
    window.addEventListener("resize", updateFullWidthElements);

    //
    // Content transformations
    //

    var nodesSnapshot = document.evaluate(
        "//*[self::h1 or self::h2 or self::h3][text()='#StreamForWellsChallenge']",
        document,
        null,
        XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
        null
    );

    for (var i = 0; i < nodesSnapshot.snapshotLength; i++) {
        var element = nodesSnapshot.snapshotItem(i);

        element.innerHTML = element.innerHTML.replace(
            /(#streamforwells)(challenge)/i,
            "$1<strong class='red'>$2</strong>"
        );
    }
});
